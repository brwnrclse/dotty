# Dotty!

###Dot files and configurations for my linux setups (debian or arch based)

Includes:

 + Zsh (oh-my-zsh)
     * Plugins
     * Syntax highlighting
     * Folder navigation (z)
 
 + Linters
     * Stylus => stylint
     * Javascript => eslint (babel-eslint)
     * Ruby => rubocop
     * Python => pylint

 + Awesome WM
     * multicolor theme

 + Sublime Text 3
     * Plugins
     * Color Schemes
     * Custom Builds
     * Language Definitions (for syntax highlighting)

 + VIM
     * Use with vim-airline

 + Miscelleanous
     *  Chmod codes (morse)
     *  Package list

 + Chromebook
     * Touchpad & Intel Graphics config
