# ----------------------------------------------------
# Aliases
#   - MIPS
#   - Network
#   - NPM
#   - Bundler
#   - Pip
#   - Pacman/Yaourt
#   - Systemd
#   - (?)
# ----------------------------------------------------

# MIPS
alias spimf='spim -file' # compile MIPS files

# Network
alias nyan='nc -v nyancat.dakko.us 23' # Nyan Cat!
alias sw='telnet towel.blinkenlights.nl' # Star Wars Episode IV, in ascii
alias net='netctl-auto list | grep "*"' # Get a list of all auto connect networks
alias neswh="sudo netctl-auto switch-to" # Switch between netctl profiles

# NPM
alias nr='npm run' # Run scripts in package.json
alias ng='npm i -g' # Install npm package globally
alias ns='npm i -S' # Install npm package to dependencies
alias nd='npm i -D' # Install npm package to devDependencies

# Bundler
alias br='bundle exec rake' # Run rake in the project's environment
alias brake='bundle exec rake' # Run rake in the project's environment
alias bex='bundle exec' # Run a command in the project's environment

# Pip
alias pipup="sudo pip freeze --local | grep -v '^\-e' | cut -d = -f 1  | xargs sudo pip install -U" # Upgrade pip packages

# Pacman
alias pacup='sudo pacman -Syu' # update all packages
alias pacen='sudo pacman -Sc' # clean cache, remove unsued repos
alias pacor='sudo pacman -Rns $(sudo pacman -Qtdq)'
alias pacin='sudo pacman -Sy' # install a package
alias pacrem='sudo pacman -R' # remove a package
alias yup='yaourt -Syu --aur' # update all packages including aur
alias yip='yaourt -Syu' # update all packages
alias yen='yaourt -Sc' # clean cache, including aur
alias yor='yaourt -Rns $(yaourt -Qtdq)'
alias yain='yaourt -Sy' # install a package
alias yarem='yaourt -R' # remove a package

# Systemd
alias sstatus='sudo systemctl status' # get status of process
alias sstart='sudo systemctl start' # state a process
alias sstop='sudo systemctl stop' # stop a process
alias sen='sudo systemctl enable' # start a process at boot
alias sdi='sudo systemctl disable' # stop a proess from starting at boot

# SVN
alias sup="svn up"
alias svdiff="svn diff"

# ?
alias death='(nohup mplayer ~/.death/.death.mp4 >/dev/null &) && exit'
alias fuck='eval $(thefuck $(fc -ln -1 | tail -n 1)); fc -R'
alias cls='clear' # haha
