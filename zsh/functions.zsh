# ----------------------------------------------------
# Functions
# ----------------------------------------------------

# Print the bitrate of a song
bitrate () {
    echo `basename "$1"`: `file "$1" | sed 's/.*, \(.*\)kbps.*/\1/' | tr -d " " ` kbps
}

# GIF to MP4
gifeo () {
    ffmpeg -i $1 -b 2048k $2
}
